package nl.bioinf.spraak.api;

/**
 * Initiated by Marcel Zanberg
 * Extended by Bas Kasemir
 */

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.WebApplicationContext;

import static nl.bioinf.spraak.api.auth.SecurityConstants.HEADER_STRING;
import static nl.bioinf.spraak.api.auth.SecurityConstants.TOKEN_PREFIX;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
@AutoConfigureMockMvc
public class ApiApplicationTests {

    private static final String TOKEN = "gvbnmvbn";
    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("build/generated-snippets");

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }


    @Test
    public void contextLoads() throws Exception{
        testMyResource();
    }

    private void testMyResource() throws Exception {

        String responseJson = "[{\"id\": 1,\"patientNumber\": \"1234\",\"soundFile\": \"1234_14--05-2019_0052S1\",\"userName\": \"VoppelA\",\"nsyll\": 1164,\"npause\": 119,\"duration\": 0,\"phonationTime\": 239.98,\"speechRate\": 3.64,\"articulation\": 4.85,\"date\": \"2019-05-20\",\"asd\": 0.206}]";

//        this.mockMvc.perform(post("http://localhost:8090/api/v1/results/praat/get/patid/{patientID}/date/?start={dateStart}&end={dateEnd}", "1234", "2019-03-01", "2019-05-21")
//                .header("Authorization", TOKEN_PREFIX+TOKEN))
//                .andExpect(status().isOk())
//                .andExpect(content().json(responseJson))
//                .andDo(document("date",
//                        requestParameters(
//                                pathVariableWithN>ame(""),
//                                parameterWithName("start").description("Start date, formatted in YYYY-MM-DD"),
//                                parameterWithName("end").description("End date, formatted in YYYY-MM-DD")
//                        ),
//                        responseFields()));
    }

}
