package nl.bioinf.spraak.api.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *  Created by Bas Kasemir
 */


public class Patient {

    private int id;
    private String patient_number;
    private String sex;
    private String birthdate;
    private String degree;
    private String diag_group;
    private Timestamp added_at;
    private String added_by;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatient_number() {
        return patient_number;
    }

    public void setPatient_number(String patient_number) {
        this.patient_number = patient_number;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDiag_group() {
        return diag_group;
    }

    public void setDiag_group(String diag_group) {
        this.diag_group = diag_group;
    }

    public Timestamp getAdded_at() {
        return added_at;
    }

    public void setAdded_at(Timestamp added_at) {
        this.added_at = added_at;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public Patient(int id, String patient_number, String sex, String birthdate, String degree, String diag_group, Timestamp added_at, String added_by) {
        this.id = id;
        this.patient_number = patient_number;
        this.sex = sex;
        this.birthdate = birthdate;
        this.degree = degree;
        this.diag_group = diag_group;
        this.added_at = added_at;
        this.added_by = added_by;
    }

    public Patient(){

    }


}
