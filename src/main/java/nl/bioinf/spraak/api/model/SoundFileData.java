package nl.bioinf.spraak.api.model;

/**
 *  created by Bas Kasemir, altered by Marcel Zandberg and Bas Kasemir
 */

import java.sql.Blob;

public class SoundFileData {

    private int id;
    private String filename;
    private Blob soundFileData;
    private String soundFileDataBase64;
    private String channel;
    private int soundfile_id_1;
    private int soundfile_id_2;



    public int getSoundfile_id_1() {
        return soundfile_id_1;
    }

    public void setSoundfile_id_1(int soundfile_id_1) {
        this.soundfile_id_1 = soundfile_id_1;
    }

    public int getSoundfile_id_2() {
        return soundfile_id_2;
    }

    public void setSoundfile_id_2(int Soundfile_id_1) {
        this.soundfile_id_2 = Soundfile_id_1;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Blob getSoundFileData() {
        return soundFileData;
    }

    public void setSoundFileData(Blob soundFileData) {
        this.soundFileData = soundFileData;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoundFileDataBase64() {
        return soundFileDataBase64;
    }

    public void setSoundFileDataBase64(String soundFileDataBase64) {
        this.soundFileDataBase64 = soundFileDataBase64;
    }

    public SoundFileData(int id, String filename, Blob soundFileData, String soundFileDataBase64, String channel, int soundfile_id_1, int Soundfile_id_1) {
        this.id = id;
        this.filename = filename;
        this.soundFileData = soundFileData;
        this.soundFileDataBase64 = soundFileDataBase64;
        this.channel = channel;
        this.soundfile_id_1 = soundfile_id_1;
        this.soundfile_id_2 = Soundfile_id_1;
    }

    public SoundFileData() {
    }
}