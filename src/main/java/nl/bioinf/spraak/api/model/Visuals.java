package nl.bioinf.spraak.api.model;

/**
 * Copyright 2020 Marcel Zandberg.
 */

public class Visuals {

    private int id;
    private String patientNumber;
    private String sex;
    private String added_at;
    private String degree;
    private String diag_group;
    private String birthdate;
    private String added_by;
    private int lowest_clipping_id;
    private String channel;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatientNumber() {
        return patientNumber;
    }

    public void setPatientNumber(String patientNumber) {
        this.patientNumber = patientNumber;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getAdded_at() {
        return added_at;
    }

    public void setAdded_at(String added_at) {
        this.added_at = added_at;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getDiag_group() {
        return diag_group;
    }

    public void setDiag_group(String diag_group) {
        this.diag_group = diag_group;
    }

    public int getLowest_clipping_id() {
        return lowest_clipping_id;
    }

    public void setLowest_clipping_id(int lowest_clipping_id) {
        this.lowest_clipping_id = lowest_clipping_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Visuals(int id, String patientNumber, String sex, String added_at, String degree, String diag_group, String birthdate, String added_by, int lowest_clipping_id, String channel) {
        this.id = id;
        this.patientNumber = patientNumber;
        this.sex = sex;
        this.added_at = added_at;
        this.degree = degree;
        this.diag_group = diag_group;
        this.birthdate = birthdate;
        this.added_by = added_by;
        this.lowest_clipping_id = lowest_clipping_id;
        this.channel = channel;
    }

    public Visuals() {
    }
}
