package nl.bioinf.spraak.api.data;

/**
 * Created by Marcel Zandberg, API implementation by Bas Kasemir
 */

import nl.bioinf.spraak.api.model.Patient;
import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.SoundFileMeta;
import nl.bioinf.spraak.api.model.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

/**
 * The interface Data source.
 */
public interface DataSource {


    List<Visuals> getVisualsResults(String startdate, String enddate, String startage, String endage, String gender, String degree, String username, String patid, String diagnosis);

    List<Visuals> getVisualsResultsOne(String startdate, String enddate, String startage, String endage, String gender, String degree, String username, String patid, String diagnosis);

    List<SoundFileData> getSoundfileByDBID(String databaseID, String username);

    int deleteMetaData(String id);

    int deleteAudio2(int id);

    int deleteAudio1(int id);

    List<SoundFileData> getSoundFileId(String id);

    int insertPraatMaxUpload();

    Patient getPatientByID(String patientID);

    List<Patient> getAllPatients();

    int insertPatient(Principal principal, Patient patientData);

    int insertSoundfileData(SoundFileData soundfileData);

    int insertSoundfileMeta(SoundFileMeta soundFileMeta);

    int getLastInsertedID();

}
