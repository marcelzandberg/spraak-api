package nl.bioinf.spraak.api.data;

/**
 * Created by Marcel Zandberg, API implementation by Bas Kasemir
 */

import nl.bioinf.spraak.api.model.Patient;
import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.SoundFileMeta;
import nl.bioinf.spraak.api.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Data source jdbc.
 */
@Component
public class DataSourceJdbc implements DataSource {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    final String INSERT_SOUNDFILE_QUERY = "INSERT INTO SpraakAPI_soundfile_data (`filename`, `soundFileData`) values (?,?)";
    final String INSERT_PATIENT_QUERY = "INSERT INTO SpraakAPI_patients (`added_by`, `patient_number`, `sex`, `birthdate`, `degree`, `diag_group`) values (?,?,?,?,?,?)";
    final String INSERT_SOUNDFILE_META_QUERY = "INSERT INTO SpraakAPI_soundfile_meta (`patient_number`, `channel`, `lowest_clipping_id`, `soundfile_id_1`, `soundfile_id_2`, `added_by`, `interviewer`) values (?,?,?,?,?,?,?)";
    final String GET_LAST_ID_QUERY = "SELECT LAST_INSERT_ID();";

    final String DELETE_AUDIO_QUERY = "delete SpraakAPI_soundfile_data from SpraakAPI_soundfile_data where SpraakAPI_soundfile_data.id = ?";

    final String DELETE_META_QUERY = "delete SpraakAPI_soundfile_meta from SpraakAPI_soundfile_meta where SpraakAPI_soundfile_meta.lowest_clipping_id = ?";

    /**
     * Instantiates a new Data source jdbc.
     *
     * @param jdbcTemplate      the jdbc template
     * @param namedJdbcTemplate the named jdbc template
     */
    @Autowired
    public DataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public int insertPraatMaxUpload() {
        return jdbcTemplate.update("SET GLOBAL max_allowed_packet=1048576000");
    }

    @Override
    public int deleteMetaData(String id){
        return jdbcTemplate.update(DELETE_META_QUERY, Integer.parseInt(id));
    }

    @Override
    public int deleteAudio1(int id){
        return jdbcTemplate.update(DELETE_AUDIO_QUERY, id);
    }

    @Override
    public int deleteAudio2(int id){
        return jdbcTemplate.update(DELETE_AUDIO_QUERY, id);
    }

    public List<SoundFileData> getSoundFileId(String dbID){
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("dbID", dbID);
        return namedJdbcTemplate.query("SELECT SpraakAPI_soundfile_meta.soundfile_id_1, SpraakAPI_soundfile_meta.soundfile_id_2 from SpraakAPI_soundfile_meta WHERE SpraakAPI_soundfile_meta.lowest_clipping_id = '"+Integer.parseInt(dbID)+"'", parameters,
                BeanPropertyRowMapper.newInstance(SoundFileData.class));
    }

    //gets the soundfiles
    public List<SoundFileData> getSoundfileByDBID(String dbID, String username){
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("dbID", dbID).addValue("username", username);
        return namedJdbcTemplate.query ("SELECT SpraakAPI_soundfile_data.id, SpraakAPI_soundfile_data.filename, SpraakAPI_soundfile_data.soundFileData, SpraakAPI_soundfile_meta.channel FROM SpraakAPI_soundfile_data join SpraakAPI_soundfile_meta on SpraakAPI_soundfile_data.id=SpraakAPI_soundfile_meta.lowest_clipping_id WHERE SpraakAPI_soundfile_data.id = '"+Integer.parseInt(dbID)+"'", parameters,
                BeanPropertyRowMapper.newInstance(SoundFileData.class));
    }

    @Override
    public Patient getPatientByID(String patientID) {
        logger.log(Level.INFO, "Getting patient data from table for patientID "+patientID);
        return namedJdbcTemplate.queryForObject("SELECT * FROM SpraakAPI_patients WHERE SpraakAPI_patients.patient_number =:patientID",
                new MapSqlParameterSource("patientID", patientID),
                BeanPropertyRowMapper.newInstance(Patient.class));
    }

    @Override
    public List<Patient> getAllPatients() {
        logger.log(Level.INFO, "Getting all patients from patient table");
        return namedJdbcTemplate.query("SELECT * FROM SpraakAPI_patients",
                BeanPropertyRowMapper.newInstance(Patient.class));
    }

    @Override
    public int insertPatient(Principal principal, Patient patientData) {
        return jdbcTemplate.update(INSERT_PATIENT_QUERY, principal.getName(), patientData.getPatient_number(), patientData.getSex(), patientData.getBirthdate(), patientData.getDegree(), patientData.getDiag_group());
    }

    @Override
    public int insertSoundfileData(SoundFileData soundFileData) {
        return jdbcTemplate.update(INSERT_SOUNDFILE_QUERY, soundFileData.getFilename(), soundFileData.getSoundFileData());
    }

    //get all results uppon option selection
    public List<Visuals> getVisualsResults(String startdate, String enddate, String startage, String endage, String gender, String degree, String username, String patid, String diagnosis){
        String timeStart = "";
        String timeEnd = "";
        if(startdate.length() > 0){
            timeStart = "00:00:00";
            timeEnd = "23:59:59";
        }
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("RangeFromdate", startdate+timeStart).addValue("RangeTilldate", enddate+timeEnd).addValue("username", username).addValue("endage", endage).addValue("startage",startage).addValue("gender", gender).addValue("degree",degree).addValue("diagnosis", diagnosis);
        return namedJdbcTemplate.query ("SELECT SpraakAPI_soundfile_meta.id, SpraakAPI_soundfile_meta.patient_number, sex, SpraakAPI_soundfile_meta.added_at, degree, diag_group, birthdate, SpraakAPI_soundfile_meta.added_by, lowest_clipping_id, SpraakAPI_soundfile_meta.channel FROM SpraakAPI_soundfile_meta, SpraakAPI_patients WHERE degree LIKE '"+degree+"%' AND sex LIKE '"+gender+"%' AND SpraakAPI_soundfile_meta.patient_number LIKE '"+patid+"%' AND diag_group LIKE '"+diagnosis+"%' AND (birthdate BETWEEN "+startage+" and "+endage+") AND NOT (SpraakAPI_soundfile_meta.added_at > :RangeTilldate OR SpraakAPI_soundfile_meta.added_at < :RangeFromdate) AND SpraakAPI_soundfile_meta.added_by =:username AND SpraakAPI_patients.patient_number = SpraakAPI_soundfile_meta.patient_number ORDER BY patient_number", parameters,
                BeanPropertyRowMapper.newInstance(Visuals.class));
    }

    //get all results uppon option selection, list only one per patient
    public List<Visuals> getVisualsResultsOne(String startdate, String enddate, String startage, String endage, String gender, String degree, String username, String patid, String diagnosis){
        String timeStart = "";
        String timeEnd = "";
        if(startdate.length() > 0){
            timeStart = "00:00:00";
            timeEnd = "23:59:59";
        }
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("RangeFromdate", startdate+timeStart).addValue("RangeTilldate", enddate+timeEnd).addValue("username", username).addValue("endage", endage).addValue("startage",startage).addValue("gender", gender).addValue("degree",degree).addValue("diagnosis", diagnosis);
        return namedJdbcTemplate.query ("SELECT SpraakAPI_soundfile_meta.id, SpraakAPI_soundfile_meta.patient_number, sex, SpraakAPI_soundfile_meta.added_at, degree, diag_group, birthdate, SpraakAPI_soundfile_meta.added_by, lowest_clipping_id, SpraakAPI_soundfile_meta.channel FROM SpraakAPI_soundfile_meta, SpraakAPI_patients WHERE degree LIKE '"+degree+"%' AND sex LIKE '"+gender+"%' AND SpraakAPI_soundfile_meta.patient_number LIKE '"+patid+"%' AND diag_group LIKE '"+diagnosis+"%' AND (birthdate BETWEEN "+startage+" and "+endage+") AND NOT (SpraakAPI_soundfile_meta.added_at > :RangeTilldate OR SpraakAPI_soundfile_meta.added_at < :RangeFromdate) AND SpraakAPI_soundfile_meta.added_by =:username AND SpraakAPI_patients.patient_number = SpraakAPI_soundfile_meta.patient_number GROUP BY patient_number ORDER BY patient_number", parameters,
                BeanPropertyRowMapper.newInstance(Visuals.class));
    }

    @Override
    public int insertSoundfileMeta(SoundFileMeta soundFileMeta) {
        return jdbcTemplate.update(INSERT_SOUNDFILE_META_QUERY, soundFileMeta.getPatient_number(), soundFileMeta.getChannel(), soundFileMeta.getLowest_clipping_id(), soundFileMeta.getSoundfile_id_1(), soundFileMeta.getSoundfile_id_2(), soundFileMeta.getAdded_by(), soundFileMeta.getInterviewer());
    }

    @Override
    public int getLastInsertedID() {
        return jdbcTemplate.queryForObject(GET_LAST_ID_QUERY, Integer.class);
    }

}

