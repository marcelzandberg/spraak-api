package nl.bioinf.spraak.api.controllers;

/**
 * Copyright 2020 Bas Kasemir.
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.Gson;
import nl.bioinf.spraak.api.model.DownloadFile;
import nl.bioinf.spraak.api.model.Patient;
import nl.bioinf.spraak.api.service.DownloadFileService;
import nl.bioinf.spraak.api.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Store controller.
 */
@RestController
@RequestMapping("/api/v1/patient")
public class PatientController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Autowired
    PatientService patientService;

    @Autowired
    DownloadFileService downloadFileService;


    /**
     * Store praat results controller int.
     *
     * @param patientData  the patient data
     * @param principal    the principal
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @PostMapping(path = "/save", produces = "application/json")
    public int storeSoundfilesController(@RequestParam(name="formData") String patientData, Principal principal) throws IOException, SQLException {

        Gson builder = new Gson();
        Patient patient = builder.fromJson(patientData, Patient.class);

        return  patientService.insertPatient(principal, patient, false);

    }

    //         api/v1/results/soundfile/get/id/1
    @RequestMapping(path = "/get/patientnumber/{patnr}", produces = "application/json")
    public Patient getFile(@PathVariable String patnr, HttpServletResponse response) throws SQLException, IOException {

        logger.log(Level.INFO, "Request for getting patient data with Patient ID "+patnr);

        Patient repsonsePatient = null;

        try {
            repsonsePatient = patientService.getPatientByID(patnr, false);
        } catch (EmptyResultDataAccessException e) {
            repsonsePatient = new Patient();
        }

        return repsonsePatient;

    }

    //         api/v1/results/soundfile/get/id/1
    @RequestMapping(path = "/get/all", produces = "application/json")
    public List<Patient> getAllPatients(HttpServletResponse response) throws SQLException, IOException {

        logger.log(Level.INFO, "Request for getting all patient data");

        List<Patient> repsonsePatientList = null;

        try {
            repsonsePatientList = patientService.getAllPatients();
        } catch (EmptyResultDataAccessException e) {
            logger.log(Level.WARNING, "Could not get all patient data");
        }

        return repsonsePatientList;

    }

    //         api/v1/results/soundfile/get/id/1
    @PostMapping(path = "/download/all", produces = "application/json")
    public DownloadFile downloadCSVOfAllPatients(HttpServletResponse response) throws SQLException, IOException {

        logger.log(Level.INFO, "Request for getting all patient data");

        List<Patient> repsonsePatientList = null;
        DownloadFile downloadFile = new DownloadFile();

        // Look up all the patients
        try {
            repsonsePatientList = patientService.getAllPatients();
        } catch (EmptyResultDataAccessException e) {
            logger.log(Level.WARNING, "Could not get all patient data");
            return downloadFile;
        }

        Gson gson = new Gson();

        // Generate JSON of the model
        String json = gson.toJson(repsonsePatientList);

        // Convert the list to CSV
        ByteArrayOutputStream outputStream = downloadFileService.convertJSONToCSV(json);

        // Generate base64 encoded srtring of the data
        String base64Encoded = Base64.getEncoder().encodeToString(outputStream.toByteArray());

        // Set the content, contentType and the extension
        downloadFile.setDataBase64Encoded(base64Encoded);
        downloadFile.setContentType("text/csv");
        downloadFile.setExtension("csv");

        // Generate a timestamp
        String ts = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());

        // Set the filename
        downloadFile.setFilename("SPRAAK_all_patients_at_"+ts);

        return downloadFile;

    }


}