package nl.bioinf.spraak.api.controllers;

/**
 * Copyright 2020 Marcel Zandberg.
 */

import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.Visuals;
import nl.bioinf.spraak.api.service.DeleteService;
import nl.bioinf.spraak.api.service.VisualsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@RestController
@RequestMapping("/api/v1/visuals")
public class VisualsController {

    private final DeleteService deleteService;
    private final VisualsService visualsService;

    List<Visuals> apiResponse;

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * Instantiates a new Result controller.
     *
     * @param visualsService the praat service
     */
    @Autowired
    public VisualsController(VisualsService visualsService, DeleteService deleteService) {
        this.deleteService = deleteService;
        this.visualsService = visualsService;
    }


// http://example.com/users/12345/bids/?start=01/01/2012&end=01/31/2012
    // http://spraak.demo:8090/api/v1/praat/get/date/?start=2019-03-01&end=2019-05-20
    @PostMapping(path = "/praat/get/all", produces = "application/json")
    public List<Visuals> getVisualResult(@RequestParam(name="startdate") String startdate, @RequestParam(name="enddate") String enddate, @RequestParam(name="startage") String startage, @RequestParam(name="endage") String endage,@RequestParam(name="gender") String gender , @RequestParam(name="degree") String degree, @RequestParam(name="username") String username, @RequestParam(name="patid") String patid, @RequestParam(name = "diagnosis") String diagnosis) {
        logger.log(Level.INFO, "got all patient data");
        return findVisualsResult(startdate, enddate, startage, endage, gender, degree ,username, patid, diagnosis);
    }

    private List<Visuals> findVisualsResult(String startdate, String enddate, String startage, String endage, String gender, String degree,String username, String patid, String diagnosis) {
        logger.log(Level.INFO, "found praat results by options");
        return visualsService.getVisualsResults(startdate, enddate, startage, endage, gender, degree, username, patid, diagnosis, false);
    }

    @PostMapping(path = "/praat/get/one", produces = "application/json")
    public List<Visuals> getVisualResultByOne(@RequestParam(name="startdate") String startdate, @RequestParam(name="enddate") String enddate, @RequestParam(name="startage") String startage, @RequestParam(name="endage") String endage,@RequestParam(name="gender") String gender , @RequestParam(name="degree") String degree, @RequestParam(name="username") String username, @RequestParam(name="patid") String patid, @RequestParam(name = "diagnosis") String diagnosis) {
        logger.log(Level.INFO, "result by one");
        return findVisualsResultOne(startdate, enddate, startage, endage, gender, degree ,username, patid, diagnosis);
    }

    private List<Visuals> findVisualsResultOne(String startdate, String enddate, String startage, String endage, String gender, String degree,String username, String patid, String diagnosis) {
        logger.log(Level.INFO, "found results by one");
        return visualsService.getVisualsResultsOne(startdate, enddate, startage, endage, gender, degree, username, patid, diagnosis, false);
    }

    //delete audio entry
    @PostMapping(path = "delete/audio", produces = "application/json")
    public int deleteAudioController(@RequestParam(name = "id") String id) throws IOException, SQLException {
        logger.log(Level.INFO, "Delete audio file");
        int status = 0;


        List<SoundFileData> ids = deleteService.getSoundFileId(id, false);

        status += deleteService.deleteAudio1(ids.get(0).getSoundfile_id_1(), false);
        status += deleteService.deleteAudio2(ids.get(0).getSoundfile_id_2(),false);
        status += deleteService.deleteMetaData(id,false);

        return status;
    }

}
