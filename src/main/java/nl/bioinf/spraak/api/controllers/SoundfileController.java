package nl.bioinf.spraak.api.controllers;

/**
 * Copyright 2019 Bas Kasemir.
 */

import com.google.gson.Gson;
import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.SoundFileMeta;
import nl.bioinf.spraak.api.service.SoundfileService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Store controller.
 */
@RestController
@RequestMapping("/api/v1/sound")
public class SoundfileController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * The Praat service.
     */
    @Autowired
    SoundfileService soundfileService;


/**
     * Store praat results controller int.
     *
     * @param principal    the principal
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @PostMapping(path = "/save", produces = "application/json")
    public int storeSoundfilesController(@RequestParam(name="lowest_clipping_file") String lowest_clipping_file, @RequestParam(name="formData") String formData, @RequestParam(name="file1") MultipartFile file1, @RequestParam(name="file2") MultipartFile file2, Principal principal) throws IOException, SQLException {


        String lowest_clipping_file_param = "file"+lowest_clipping_file;

        Gson gson = new Gson();

        // Map the repsonse JSON to the corresponding model
        SoundFileMeta sFM = gson.fromJson(formData, SoundFileMeta.class);

        SoundFileData sFD1 = new SoundFileData();

        SoundFileData sFD2 = new SoundFileData();

        InputStream file1IS = file1.getInputStream();

        byte[] bytesOfSoundFile1 = IOUtils.toByteArray(file1IS);

        Blob blob1 = new SerialBlob(file1.getBytes());

        sFD1.setSoundFileData(blob1);
        sFD1.setFilename(file1.getOriginalFilename());



        InputStream file2IS = file1.getInputStream();

        byte[] bytesOfSoundFile2 = IOUtils.toByteArray(file2IS);

        Blob blob2 = new javax.sql.rowset.serial.SerialBlob(bytesOfSoundFile2);

        sFD2.setSoundFileData(blob2);
        sFD2.setFilename(file2.getOriginalFilename());

        soundfileService.insertSoundfileData(sFD1, false);
        int id_of_file_1 = soundfileService.getLastInsertedID();

        soundfileService.insertSoundfileData(sFD2, false);
        int id_of_file_2 = soundfileService.getLastInsertedID();

        sFM.setSoundfile_id_1(id_of_file_1);

        sFM.setSoundfile_id_2(id_of_file_2);

        if (file1.getOriginalFilename().equals(lowest_clipping_file)){
            sFM.setLowest_clipping_id(id_of_file_1);
        }

        if (file2.getOriginalFilename().equals(lowest_clipping_file)){
            sFM.setLowest_clipping_id(id_of_file_2);
        }

        sFM.setAdded_by(principal.getName());

        soundfileService.insertSoundfileMeta(sFM, false);

        return 1;

    }

}