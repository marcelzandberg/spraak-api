package nl.bioinf.spraak.api.controllers;

/**
 *  created by Marcel Zandberg
 */

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Error controller.
 */
@Controller
public class ErrorController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * Error greeting string.
     *
     * @param name     the name
     * @param model    the model
     * @param request  the request
     * @param response the response
     * @return the string
     */
    @GetMapping("/error")
    public String errorGreeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model, HttpServletRequest request, HttpServletResponse response) {
        logger.log(Level.WARNING, "Error occured");
        model.addAttribute("name", name);

        String errorMsg = "";
        int httpErrorCode = getErrorCode(request);

        switch (httpErrorCode) {
            case 400: {
                errorMsg = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401: {
                errorMsg = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 403: {
                errorMsg = "Http Error Code: 403. Access denied";
                break;
            }
            case 404: {
                errorMsg = "Http Error Code: 404. Resource not found";
                break;
            }
            case 405: {
                errorMsg = "Http Error Code: 405. Method not allowed";
                break;
            }
            case 500: {
                errorMsg = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }
        model.addAttribute("errorMsg", errorMsg);
        return "error";
    }

    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
                .getAttribute("javax.servlet.error.status_code");

    }
}
