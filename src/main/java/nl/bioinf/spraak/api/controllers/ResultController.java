package nl.bioinf.spraak.api.controllers;

/**
 * Copyright 2020 Bas Kasemir and MArcel Zandberg.
 */

import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.service.VisualsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Result controller.
 */
@RestController
@RequestMapping("/api/v1/results")
public class ResultController {

    private final VisualsService visualsService;

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * Instantiates a new Result controller.
     *
     * @param visualsService the praat service
     */
    @Autowired
    public ResultController(VisualsService visualsService) {
        this.visualsService = visualsService;
    }

    /**
     * Gets file.
     *
     * @param dbID     the db id
     * @param username the username
     * @param response the response
     * @return the file
     * @throws SQLException the sql exception
     */
//         api/v1/results/soundfile/get/id/1
    @PostMapping(path = "/soundfile/get/id/{dbID}", produces = "application/json")
    public List<SoundFileData> getFile(@PathVariable List<String> dbID, @RequestParam(name="username") String username, HttpServletResponse response) throws SQLException {

        List<SoundFileData> sfModel = findSoundfileByDBID(dbID, username);

        for(int i =0; i < sfModel.size(); i++){
            Blob blob = sfModel.get(i).getSoundFileData();

            int blobLength = (int) blob.length();
            byte[] blobAsBytes = blob.getBytes(1, blobLength);

            //release the blob and free up memory. (since JDBC 4.0)
            blob.free();

            byte[] encodedBytes = Base64.getEncoder().encode(blobAsBytes);

            sfModel.get(i).setSoundFileDataBase64(new String(encodedBytes));

            sfModel.get(i).setSoundFileData(null);
        }

        logger.log(Level.INFO, "soundfile model created");

        return sfModel;

    }

    private List<SoundFileData> findSoundfileByDBID(List<String> dbID, String username) {
        logger.log(Level.INFO, "soundfile found");

        List<SoundFileData> soundFileData = new ArrayList<>();

        for (int i = 0; i < dbID.size(); i++) {
            List<SoundFileData> data = visualsService.getSoundfileByDBID(dbID.get(i), username, false);
            for (int j = 0; j < data.size(); j++) {
                soundFileData.add(data.get(j));
            }
        }
        return soundFileData;
    }

}