package nl.bioinf.spraak.api.service;

import nl.bioinf.spraak.api.data.DataSource;
import nl.bioinf.spraak.api.model.Patient;
import nl.bioinf.spraak.api.model.SoundFileData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PatientService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Autowired
    DataSource dataSource;

    /**
     * Insert user int.
     *
     * @param patientID    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public Patient getPatientByID(String patientID, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Getting patient data for ID "+patientID);
            return dataSource.getPatientByID(patientID);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Get list of all patients and their data
     *
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public List<Patient> getAllPatients() throws IOException, SQLException {
        logger.log(Level.INFO, "Getting all patient data for ID");
        return dataSource.getAllPatients();
    }

    /**
     * Insert user int.
     *
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertPatient(Principal principal, Patient patientData, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice inseruser executed");
            return dataSource.insertPatient(principal, patientData);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

}
