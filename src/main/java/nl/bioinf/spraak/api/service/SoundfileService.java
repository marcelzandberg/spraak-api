package nl.bioinf.spraak.api.service;

import nl.bioinf.spraak.api.data.DataSource;
import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.SoundFileMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SoundfileService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    @Autowired
    DataSource dataSource;
/*

    */
/**
     * Insert user int.
     *
     * @param soundFileMeta    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */

    public int insertSoundfileMeta(SoundFileMeta soundFileMeta, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "soundFileData insert executed");
            return dataSource.insertSoundfileMeta(soundFileMeta);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Insert user int.
     *
     * @param soundfile    the soundfile data model
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertSoundfileData(SoundFileData soundfile, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Inserting soundfile");
            return dataSource.insertSoundfileData(soundfile);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Get last id
     *
     * @param soundfile    the soundfile data model
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int getLastInsertedID() throws IOException, SQLException {
            logger.log(Level.INFO, "Getting last inserted ID");
            return dataSource.getLastInsertedID();
    }

}
