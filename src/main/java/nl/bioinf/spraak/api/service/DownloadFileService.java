package nl.bioinf.spraak.api.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class DownloadFileService {

    public ByteArrayOutputStream convertJSONToCSV(String json) throws IOException {
        JsonNode jsonTree = new ObjectMapper().readTree(json);

        CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();
        JsonNode firstObject = jsonTree.elements().next();
        firstObject.fieldNames().forEachRemaining(fieldName -> {csvSchemaBuilder.addColumn(fieldName);} );
        CsvSchema csvSchema = csvSchemaBuilder.build().withHeader();

        CsvMapper csvMapper = new CsvMapper();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        csvMapper.writerFor(JsonNode.class)
                .with(csvSchema)
                .writeValue(byteArrayOutputStream, jsonTree);

        return byteArrayOutputStream;
    }

}
