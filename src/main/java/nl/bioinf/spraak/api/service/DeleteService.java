package nl.bioinf.spraak.api.service;

/**
 * Copyright 2020 Marcel Zandberg.
 */

import nl.bioinf.spraak.api.data.DataSource;
import nl.bioinf.spraak.api.model.SoundFileData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeleteService {

    private final DataSource dataSource;

    @Autowired
    public DeleteService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<SoundFileData> getSoundFileId(String id, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.getSoundFileId(id);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    public int deleteAudio1(int id, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.deleteAudio1(id);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    public int deleteAudio2(int id, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.deleteAudio2(id);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    public int deleteMetaData(String id ,boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.deleteMetaData(id);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
}
