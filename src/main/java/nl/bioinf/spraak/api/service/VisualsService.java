package nl.bioinf.spraak.api.service;

/**
 * Created by Marcel Zandberg
 */


import nl.bioinf.spraak.api.data.DataSource;
import nl.bioinf.spraak.api.model.SoundFileData;
import nl.bioinf.spraak.api.model.Visuals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;


@Service
public class VisualsService {


    private final DataSource dataSource;

    /**
     * Instantiates a new Praat service.
     *
     * @param dataSource the data source
     */
    @Autowired
    public VisualsService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    // get sound files by pat id
    public List<SoundFileData> getSoundfileByDBID(String dbID, String username, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.getSoundfileByDBID(dbID, username);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    // get all results for visuals
    public List<Visuals> getVisualsResults(String startdate, String enddate, String startage, String endage, String gender, String degree ,String username, String patid, String diagnosis, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.getVisualsResults(startdate, enddate, startage, endage, gender, degree, username, patid, diagnosis);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
    // get all results for visuals but one per patient
    public List<Visuals> getVisualsResultsOne(String startdate, String enddate, String startage, String endage, String gender, String degree ,String username, String patid, String diagnosis, boolean usePartMatching){
        if (!usePartMatching) {
            return dataSource.getVisualsResultsOne(startdate, enddate, startage, endage, gender, degree, username, patid, diagnosis);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Inser praat max upload int.
     *
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int inserPraatMaxUpload(boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            return dataSource.insertPraatMaxUpload();
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }


}

