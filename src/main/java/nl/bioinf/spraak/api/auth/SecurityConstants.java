package nl.bioinf.spraak.api.auth;

/**
 * Copyright 2019 Bas Kasemir
 */
public class SecurityConstants {
    /**
     * The constant TOKEN_PREFIX.
     */
// Sets the prefix for the token
    public static final String TOKEN_PREFIX = "Bearer ";
    /**
     * The constant HEADER_STRING.
     */
// Sets the key for the header
    public static final String HEADER_STRING = "Authorization";
}