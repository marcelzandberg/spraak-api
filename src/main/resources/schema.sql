DROP TABLE IF EXISTS SpraakAPI_soundfile_meta;
DROP TABLE IF EXISTS SpraakAPI_soundfile_data;
DROP TABLE IF EXISTS SpraakAPI_patients;

CREATE TABLE IF NOT EXISTS SpraakAPI_patients (
  id                INT AUTO_INCREMENT,
  patient_number    varchar(45) unique not null,
  sex               enum('M', 'F', 'O') not null ,
  birthdate         date not null ,
  degree            varchar(45) not null ,
  diag_group        varchar(45) not null ,
  added_at          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  added_by          varchar(45) not null,
  PRIMARY KEY (id, patient_number)
);

CREATE TABLE IF NOT EXISTS SpraakAPI_soundfile_data (
  id                    INT AUTO_INCREMENT,
  filename              varchar(45) not null,
  soundFileData         longblob not null,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS SpraakAPI_soundfile_meta (
  id                    INT AUTO_INCREMENT,
  patient_number        varchar(45) not null,
  channel               set('L', 'R') not null,
  lowest_clipping_id    int not null,
  soundfile_id_1        int not null,
  soundfile_id_2        int not null,
  added_at              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  added_by              varchar(45) not null,
  interviewer           varchar(45) not null,
  PRIMARY KEY (id)
);

ALTER TABLE SpraakAPI_soundfile_meta
   ADD FOREIGN KEY (patient_number) REFERENCES SpraakAPI_patients (patient_number),
   ADD FOREIGN KEY (lowest_clipping_id) REFERENCES SpraakAPI_soundfile_data (id),
   ADD FOREIGN KEY (soundfile_id_1) REFERENCES SpraakAPI_soundfile_data (id),
   ADD FOREIGN KEY (soundfile_id_2) REFERENCES SpraakAPI_soundfile_data (id);
